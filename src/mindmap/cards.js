import { writable, derived } from 'svelte/store';

//Cards Array

function createCards(){
	const { subscribe, set, update } = writable([]);

	const card = {  
		id: 0,
		cood: { x: 40, y: 40 },
		size: { w: 20, h: 20 }
	}

	return {
		subscribe,

		newCard: () => update( cards =>{
			card.id = cards.length;
			return [ ...cards, card ];
		}),	

		moveCard: ( card, i )=> update( cards =>{
			cards[ i ] = card;
			return [ ...cards ];  
		}) 
	}
}

export const cards = createCards();

//Selected Card Object

function selectCard(){
	const { subscribe, set, update } = writable({ id: -1, offset: Object() });

	return {
		subscribe, 
		card: ( e, cood )=> {
			let ctm = e.target.getScreenCTM( );

			update( selected => {
				return { 
					id: e.target.id, 
					offset: { 
						x: ( e.clientX - ctm.e ) / ctm.a - parseFloat( cood.x ),
						y: ( e.clientY - ctm.f ) / ctm.d - parseFloat( cood.y )
					}
				}
			})
		},
		reset: ( ) => set({ id: -1, offset: new Object() })
	};
};

export const selected = selectCard();


